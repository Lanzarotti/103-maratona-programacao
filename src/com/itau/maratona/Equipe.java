package com.itau.maratona;

import java.util.ArrayList;

public class Equipe {
	public int id;
	public ArrayList<Aluno> alunos = new ArrayList<>();
	
	@Override
	public String toString() {
		String texto = "";
		
		texto += "Id: " + id;
		texto += "\n";
		
		for(Aluno aluno: alunos) {
			texto += aluno.nome;
			texto += "\n";
		}
		
		return texto;
	}
}

